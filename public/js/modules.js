import sing from "HW/HW-1/sing"
import hunter from "HW/HW-1/hunter"
import basicOnes from "HW/HW-1/basicOnes"
import swim from "HW/HW-1/swim"

const nightingale = Object.assign({}, basicOnes, sing);
const penguin = Object.assign({}, basicOnes, swim);
const eagle = Object.assign({}, basicOnes, hunter);
